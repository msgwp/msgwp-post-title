All notable changes to this project will be documented in this file.

### 0.5.0 (2019-02-06)

#### Added
- Before activation we check if msgWP (core) is installed and activated.

### 0.4.0 (2019-02-05)

#### Added
- There is a new filter for changing the slug type (post_name).

### 0.3.0 (2019-02-05)

#### Added
- The plugin can be updated using the Plugin Update Checker.

### 0.2.0 (2019-02-05)

#### Changed
- The UID is used for the post_name (slug). 

### 0.1.0 (2019-02-05)

#### Added
- The basic functionality to split the text into title and body is added.