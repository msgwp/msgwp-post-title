<?php
/*
 * Plugin Name: msgWP: Post Title
 * Description: Set your post title…
 * Plugin URI:  https://msgwp.com
 * Version:     0.5.0
 * Author:      implenton
 * Author URI:  https://implenton.com
 * License:     GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 */

namespace msgWP\AddOn\PostTitle;

use function msgWP\generate_post_uid_from_message;
use Puc_v4_Factory;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

register_activation_hook( __FILE__, __NAMESPACE__ . '\plugin_activation' );

function plugin_activation() {
    // TODO: Internationalize the plugin activation errors
    if ( ! defined( 'MSGWP_VERSION' ) ) {
        wp_die( 'You have to install and activate <a href="https://msgwp.com" target="_blank" rel="noopener">msgWP</a> first.', '' );
    }
}

require_once dirname( __FILE__ ) . '/vendor/yahnis-elsts/plugin-update-checker/plugin-update-checker.php';

add_filter( 'msgwp/handler/text/post_args', __NAMESPACE__ . '\set_title', 5, 2 );
add_filter( 'msgwp/handler/edited_text/post_args', __NAMESPACE__ . '\set_title', 5, 2 );

function set_title( $prev_args, $payload ) {
    $text_parts = split_text_into_title_body( $payload['data']['text'] );
    $uid        = generate_post_uid_from_message( $payload );

    if ( is_null( $text_parts ) ) {
        return $prev_args;
    }

    switch ( apply_filters( 'msgwp/addon/post_title/slug', 'uid' ) ) {
        case 'title':
            $slug = null;
            break;
        case 'always_title':
            $slug = sanitize_title( $text_parts['title'] );
            break;
        default:
            $slug = $uid;
    }

    return wp_parse_args( [
        'post_title'   => $text_parts['title'],
        'post_content' => $text_parts['body'],
        'post_name'    => $slug,
    ], $prev_args );
}

function split_text_into_title_body( $content ) {
    $delimiter  = apply_filters( 'msgwp/addon/post_title/delimiter', '----' );
    $text_parts = explode( $delimiter, $content, 2 );

    return count( $text_parts ) === 1 ? null : [
        'title' => trim( $text_parts[0] ),
        'body'  => trim( $text_parts[1] ),
    ];
}

if ( class_exists( 'Puc_v4_Factory' ) ) {
    $update_checker = Puc_v4_Factory::buildUpdateChecker(
        'https://gitlab.com/msgwp/msgwp-post-title/',
        __FILE__,
        'msgwp-post-title'
    );
}